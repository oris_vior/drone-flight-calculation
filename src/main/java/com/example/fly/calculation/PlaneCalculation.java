package com.example.fly.calculation;

import com.example.fly.object.AirplaneCharacteristics;
import com.example.fly.object.TemporaryPoint;
import com.example.fly.object.WayPoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlaneCalculation {
    static Boolean collinear = false;
    static Boolean changeDirection = false;

    public static boolean fromDifferentSides(Vector main, Vector v1, Vector v2) {
        double product1 = Vector.crs(main, v1), product2 = Vector.crs(main, v2);
        return (product1 >= 0 && product2 <= 0 || product1 <= 0 && product2 >= 0);
    }

    public List<TemporaryPoint> calculateRoute(AirplaneCharacteristics characteristics, List<WayPoint> wayPoints) {
        double currentCourse = 90;

        List<TemporaryPoint> temporaryPointList = new ArrayList<>();
        for (int i = 0; i < wayPoints.size() - 1; i++) {

            temporaryPointList.addAll(positionAfterOneSecond(characteristics, wayPoints.get(i), wayPoints.get(i + 1), currentCourse)) ;
            collinear = false;
            changeDirection = false;
        }
        return temporaryPointList;
    }

    private List<TemporaryPoint> positionAfterOneSecond(AirplaneCharacteristics characteristics, WayPoint wayPointStart, WayPoint wayPointEnd, double currentCourse) {
        List<TemporaryPoint> list = new ArrayList<>();
        double positionLatitude = wayPointStart.getLatitude();
        double positionLongitude = wayPointStart.getLongitude();
        double previousPositionLatitude = positionLatitude - 1;
        double previousPositionLongitude = positionLongitude - 1;
        double currentSpeed = wayPointStart.getFlyingSpeed();
        double currentHeight = wayPointStart.getSpanHeight();
        double angleBetweenXAndWayPointCourse = getAngle(wayPointStart, wayPointEnd, 1, 0, 0, 0);
        String position = "";

        if (angleBetweenXAndWayPointCourse > currentCourse) {
            position = "right";
        } else if (angleBetweenXAndWayPointCourse < currentCourse) {
            position = "left";
        }

        while (waypointCheck(positionLatitude, positionLongitude, previousPositionLatitude, previousPositionLongitude, wayPointEnd)) {

            previousPositionLatitude = positionLatitude;
            previousPositionLongitude = positionLongitude;
            positionLatitude = getNewCoordinate(currentSpeed, currentCourse, positionLatitude, positionLongitude).get(0);
            positionLongitude = getNewCoordinate(currentSpeed, currentCourse, positionLatitude, positionLongitude).get(1);

            double previousSpeed = currentSpeed;
            currentSpeed = getNewValueSpeed(currentSpeed, wayPointEnd.getFlyingSpeed(), characteristics.getMaximumAcceleration());
            currentHeight = getNewHeight(currentHeight, characteristics.getHeightChangeRate(), wayPointEnd.getSpanHeight());

            double angleBetweenActualCurseAndDirection = getAngle(wayPointStart, wayPointEnd, positionLatitude, positionLongitude, previousPositionLatitude, previousPositionLongitude);
            if (previousSpeed == currentSpeed) {
                currentCourse = getNewCourse(characteristics.getRateOfChangeOfCourse(), angleBetweenActualCurseAndDirection, angleBetweenXAndWayPointCourse, currentCourse, position, wayPointStart, wayPointEnd, positionLatitude, positionLongitude, previousPositionLatitude, previousPositionLongitude);
            }
            TemporaryPoint temp = new TemporaryPoint(positionLatitude, positionLongitude, currentHeight, currentSpeed, currentCourse);
            list.add(temp);
            System.out.println(temp.getLatitude() + ";" + temp.getLongitude());
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        return list;
    }

    private double getNewCourse(double rateOfChangeOfCourse, double angleBetweenActualCurseAndDirection, double angleBetweenXAndWayPointCourse, double currentCourse, String position, WayPoint wayPointStart, WayPoint wayPointEnd, double positionLatitude, double positionLongitude, double previousPositionLatitude, double previousPositionLongitude) {
        if (angleBetweenXAndWayPointCourse > currentCourse && !collinear) {
            currentCourse = currentCourse + rateOfChangeOfCourse;
        } else if (angleBetweenXAndWayPointCourse < currentCourse && !collinear) {
            currentCourse = currentCourse - rateOfChangeOfCourse;
        } else if (angleBetweenActualCurseAndDirection == 0 && !changeDirection) {
            System.out.println("parallel");
            collinear = true;
            currentCourse = courseCorrection(currentCourse, position, rateOfChangeOfCourse);
            return currentCourse;
        }
        if (lineIntersection(wayPointStart, wayPointEnd, positionLatitude, positionLongitude, previousPositionLatitude, previousPositionLongitude)) {
            changeDirection = true;
            return angleBetweenXAndWayPointCourse;
        }
        if (lineIntersection(wayPointStart, wayPointEnd, positionLatitude, positionLongitude, previousPositionLatitude, previousPositionLongitude)) {
            changeDirection = true;
            return angleBetweenXAndWayPointCourse;
        }
        if (changeDirection && angleBetweenActualCurseAndDirection == 0) {
            changeDirection = false;
            double lastAngle = getAngle(wayPointStart, wayPointEnd, wayPointEnd.getLatitude(), wayPointEnd.getLongitude(), positionLatitude, positionLongitude);
            if (lastAngle > 0 && position.equals("right")) {
                return currentCourse - lastAngle;
            } else if (lastAngle > 0 && position.equals("left")) {
                return currentCourse + lastAngle;
            }
        }

        return currentCourse;
    }

    private double courseCorrection(double currentCourse, String position, double rateOfChangeOfCourse) {
        if (position.equals("right") && !changeDirection) {
            return currentCourse +  rateOfChangeOfCourse;
        } else if (position.equals("left") && !changeDirection) {
            return currentCourse -  rateOfChangeOfCourse;
        }
        return currentCourse;
    }

    private boolean lineIntersection(WayPoint wayPointStart, WayPoint wayPointEnd, double positionLatitude, double positionLongitude, double previousPositionLatitude, double previousPositionLongitude) {
        Vector main = new Vector(wayPointStart.getLatitude(), wayPointStart.getLongitude(), wayPointEnd.getLatitude(), wayPointEnd.getLongitude());
        Vector v1 = new Vector(wayPointStart.getLatitude(), wayPointStart.getLongitude(), previousPositionLatitude, previousPositionLongitude);
        Vector v2 = new Vector(wayPointStart.getLatitude(), wayPointStart.getLongitude(), positionLatitude, positionLongitude);

        if (fromDifferentSides(main, v1, v2)) {
            main = new Vector(previousPositionLatitude, previousPositionLongitude, positionLatitude, positionLongitude);
            v1 = new Vector(previousPositionLatitude, previousPositionLongitude, wayPointStart.getLatitude(), wayPointStart.getLongitude());
            v2 = new Vector(previousPositionLatitude, previousPositionLongitude, wayPointEnd.getLatitude(), wayPointEnd.getLongitude());
            return fromDifferentSides(main, v1, v2);
        }
        return false;
    }

    //проверка на принадлежность пройденого отрезка к way point
    private boolean waypointCheck(double positionLatitude, double positionLongitude, double previousPositionLatitude, double previousPositionLongitude, WayPoint wayPointEnd) {

        if ((wayPointEnd.getLatitude() - previousPositionLatitude) + 1 / (positionLatitude - previousPositionLatitude) + 1 == (wayPointEnd.getLongitude() - previousPositionLongitude) + 1 / (positionLongitude - previousPositionLongitude) + 1 && wayPointEnd.getLatitude() >= previousPositionLatitude && wayPointEnd.getLatitude() <= positionLatitude && wayPointEnd.getLongitude() >= previousPositionLongitude && wayPointEnd.getLongitude() <= positionLongitude || wayPointEnd.getLatitude() - positionLatitude <= 50 && wayPointEnd.getLatitude() - positionLatitude >= -50 && wayPointEnd.getLongitude() - positionLongitude <= 50 && wayPointEnd.getLongitude() - positionLongitude >= -50) {
            return false;
        }
        return true;
    }

    private List<Double> getNewCoordinate(double speed, double currentCourse, double positionLatitude, double positionLongitude) {

        double scale = Math.pow(10, 3);
        positionLatitude = positionLatitude + Math.ceil(Math.cos(Math.toRadians(currentCourse)) * scale) / scale * speed;
        positionLongitude = positionLongitude + Math.ceil(Math.sin(Math.toRadians(currentCourse)) * scale) / scale * speed;
        return Arrays.asList(positionLatitude, positionLongitude);
        //}
    }

    private double getAngle(WayPoint wayPointStart, WayPoint wayPointEnd, double positionLatitude, double positionLongitude, double previousPositionLatitude, double previousPositionLongitude) {
        double vectorWayPointLatitude = wayPointEnd.getLatitude() - wayPointStart.getLatitude();
        double vectorWayPointLongitude = wayPointEnd.getLongitude() - wayPointStart.getLongitude();
        double currentCourseVectorLatitude = positionLatitude - previousPositionLatitude;
        double currentCourseVectorLongitude = positionLongitude - previousPositionLongitude;
        double dotProductOfVector = vectorWayPointLatitude * currentCourseVectorLatitude + vectorWayPointLongitude * currentCourseVectorLongitude;
        double absWaypointVector = Math.sqrt(Math.pow(vectorWayPointLatitude, 2) + Math.pow(vectorWayPointLongitude, 2));
        double absCurrentCourseVector = Math.sqrt(Math.pow(currentCourseVectorLatitude, 2) + Math.pow(currentCourseVectorLongitude, 2));
        return Math.round(Math.toDegrees(Math.acos(dotProductOfVector / (absWaypointVector * absCurrentCourseVector))));
    }

    private double getNewValueSpeed(double currentSpeed, double flyingSpeed, double heightChangeRate) {
        if (currentSpeed < flyingSpeed) {
            currentSpeed += heightChangeRate;
            return currentSpeed;
        } else if (currentSpeed > flyingSpeed) {
            currentSpeed -= heightChangeRate;
            return currentSpeed;
        } else return currentSpeed;
    }

    private double getNewHeight(double currentHeight, double heightChangeRate, double spanHeight) {
        if (currentHeight < spanHeight) {
            return currentHeight + heightChangeRate;
        } else if (currentHeight > spanHeight) {
            return currentHeight - spanHeight;
        } else return currentHeight;
    }

    public static class Vector {
        double x, y;

        public Vector(double x1, double y1, double x2, double y2) {
            this.x = x2 - x1;
            this.y = y2 - y1;
        }

        /**
         * Calculates the 2D cross product between this and the given vector.
         *
         * @param v2 the other vector
         * @return the cross product (Z vector)
         */
        public static double crs(Vector v1, Vector v2) {
            return v1.x * v2.y - v1.y * v2.x;
        }
    }
}
