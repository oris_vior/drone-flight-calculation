package com.example.fly.beens;

import com.example.fly.calculation.PlaneCalculation;
import com.example.fly.object.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Configuration
@EnableScheduling
public class TestBeen {
    private  Long id = 0L;
    private AirplaneCharacteristics characteristic = new AirplaneCharacteristics(
            200,
            40,
            50,
            15);
    Airplane airplane1 = new Airplane(1L ,
            characteristic,
            new TemporaryPoint(0,0,0,0,90)
            ,new ArrayList<Flight>());
    @Scheduled(fixedDelay = 2000)
    public void executeTask1() {
        System.out.println("кількість польотів: " + airplane1.getId());
        if(airplane1.getFlights().size() != 0){
        System.out.println("час останього польоту: " + airplane1.getFlights().get(airplane1.getFlights().size() - 1).getPassedPoints().size());}
        else {
            System.out.println("час останього польоту: Польотів не було");
        }
        PlaneCalculation planeCalculation = new PlaneCalculation();
        WayPoint point0 = new WayPoint(0, 0, 0, 0);
        WayPoint point1 = new WayPoint(2000, 2000, 300, 120);
        List<TemporaryPoint> temporaryPointList = planeCalculation.calculateRoute(characteristic, List.of(point0, point1));

        Flight flight = new Flight(id, List.of(new WayPoint(0, 0, 0, 0),
                new WayPoint(2000, 2000, 300, 120)), temporaryPointList);
        airplane1.getFlights().add(flight);
        id += 1L;


    }
}
