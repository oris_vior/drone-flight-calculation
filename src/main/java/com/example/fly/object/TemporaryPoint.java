package com.example.fly.object;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class TemporaryPoint {
    private double Latitude;
    private double Longitude;
    private double SpanHeight;
    private double FlyingSpeed;
    private double CourseDegree;

}
