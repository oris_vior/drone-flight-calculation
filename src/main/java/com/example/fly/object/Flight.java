package com.example.fly.object;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor

public class Flight {
    private Long number;
    private List<WayPoint> wayPointList;
    private List<TemporaryPoint> passedPoints;
}
