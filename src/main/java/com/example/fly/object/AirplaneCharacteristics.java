package com.example.fly.object;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class AirplaneCharacteristics {
    private double MaxSpeed;
    private double MaximumAcceleration;
    private double HeightChangeRate;
    private double RateOfChangeOfCourse;
}
