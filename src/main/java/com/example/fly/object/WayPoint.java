package com.example.fly.object;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WayPoint {
    private double Latitude; //Широта - x
    private double Longitude; // Долгота - y
    private double SpanHeight;
    private double FlyingSpeed;

}
